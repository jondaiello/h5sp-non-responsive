<IfModule mod_rewrite.c>
        RewriteEngine On

        # Removes index.php
        RewriteCond $1 !\.(gif|jpe?g|png)$ [NC]
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteRule ^(.*)$ /index.php/$1 [L]

        # If 404s, "No Input File" or every URL returns the same thing
        # make it /index.php?/$1 above (add the question mark)
</IfModule>


#<IfModule mod_expires.c>
#    # Activate mod_expires for this directory
#    ExpiresActive on
#
#    # locally cache common image types for 30 days
#    ExpiresByType image/jpg "access plus 30 days"
#    ExpiresByType image/jpeg "access plus 30 days"
#    ExpiresByType image/gif "access plus 30 days"
#    ExpiresByType image/png "access plus 30 days"
#    ExpiresByType image/x-icon "access plus 30 days"
#    ExpiresByType image/ico "access plus 30 days"

    # cache CSS and JS files for 24 hours
#    ExpiresByType text/css "access plus 7 days"
#    ExpiresByType application/x-javascript "access plus 30 days"
#    ExpiresByType application/javascript "access plus 30 days"
#</IfModule>


#<IfModule mod_deflate.c>
#  AddOutputFilterByType DEFLATE text/text text/html text/php text/plain text/xml text/css application/x-javascript application/javascript
#</IfModule>

#FileETag none