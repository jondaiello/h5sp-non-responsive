$(function() {
	// CLEAR SEARCH BOXES
	$('input, textarea').each(function() {
		var default_value = this.value;
		$(this).focus(function() {
			if(this.value == default_value) {
			this.value = '';
			}
		});
		$(this).blur(function() {
			if(this.value == '') {
			this.value = default_value;
			}
		});
	});
});


// Responsive Slides
$(function () {
  $("#slider").responsiveSlides( {
    auto: true,
    pager: true,
    nav: true,
    speed: 200,
    timeout: 11000,
    namespace: "slides"
  });
});


// dropkick
//$('select.styled').dropkick({startSpeed:0});


// tabs in profile section
//$("section.profile-details").tabs();